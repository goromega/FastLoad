//
//  FastLoadTests.m
//  FastLoadTests
//
//  Created by Gor Hakobyan on 9/4/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FLWebServerManager.h"
#import "NSString+SHA1.h"
#import "UIColor+FLAdditions.h"
#import "UIFont+FLFonts.h"


@interface FastLoadTests : XCTestCase

@end

@implementation FastLoadTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

- (void)testSHA1 {
    NSString *gor = [NSString stringToSha1:@"gor"];
    XCTAssert(gor, @"fd6608149f5f589003abf198bcd5ef1b94afa26b");
}


- (void)testColors {
    XCTAssertNotNil([UIColor fl_darkGreenColor]);
    XCTAssertNotNil([UIColor fl_backgroundColor]);
    XCTAssertNotNil([UIColor fl_lightGreenColor]);
}

- (void)testFonts{
    XCTAssertNotNil([UIFont openSansWithSize:14]);
    XCTAssertNotNil([UIFont openSansBoldWithSize:14]);
    XCTAssertNotNil([UIFont openSansLightWithSize:14]);
    XCTAssertNotNil([UIFont openSansItalicWithSize:14]);
    XCTAssertNotNil([UIFont openSansSemiboldWithSize:14]);
    XCTAssertNotNil([UIFont openSansExtraBoldWithSize:14]);
    XCTAssertNotNil([UIFont openSansBoldItalicWithSize:14]);
    XCTAssertNotNil([UIFont openSansSemiboldItalicWithSize:14]);
}

@end
