//
//  UIColor+FLAdditions.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/4/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "UIColor+FLAdditions.h"


@implementation UIColor (FLAdditions)

+ (UIColor *)fl_colorFromHexString:(NSString *)hexString alpha:(CGFloat)alpha {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:alpha];
    
}

+ (UIColor *)fl_darkGreenColor {
    return [UIColor fl_colorFromHexString:@"#1b7d74" alpha:1];
}

+ (UIColor *)fl_lightGreenColor {
    return [UIColor fl_colorFromHexString:@"#bfd8ca" alpha:1];
}

+ (UIColor *)fl_backgroundColor {
    return [UIColor fl_colorFromHexString:@"#292f37" alpha:1];
}

@end
