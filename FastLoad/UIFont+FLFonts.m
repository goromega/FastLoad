//
//  UIFont+FLFonts.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/7/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "UIFont+FLFonts.h"
#import <CoreText/SFNTLayoutTypes.h>

@implementation UIFont (FLFonts)

#pragma mark Open Sans
+ (UIFont *) openSansWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"OpenSans" size:size];
}
+ (UIFont *) openSansBoldItalicWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"OpenSans-BoldItalic" size:size];
}
+ (UIFont *) openSansItalicWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"OpenSans-Italic" size:size];
}

+ (UIFont *) openSansBoldWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"OpenSans-Bold" size:size];
}

+ (UIFont *) openSansLightWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"OpenSans-Light" size:size];
}

+ (UIFont *) openSansLightItalicWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"OpenSans-LightItalic" size:size];
}

+ (UIFont *) openSansSemiboldWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"OpenSans-Semibold" size:size];
}

+ (UIFont *) openSansSemiboldItalicWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"OpenSans-SemiboldItalic" size:size];
}

+ (UIFont *) openSansExtraBoldWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"OpenSans-ExtraBold" size:size];
}

@end
