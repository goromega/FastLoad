//
//  FLRequiredTextValidator.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/4/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "FLRequiredTextValidator.h"

@implementation FLRequiredTextValidator

- (BOOL)validate {
    return ([self.text length] > 0);
}

@end
