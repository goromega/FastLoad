//
//  FLLoginFieldDelegate.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/5/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "FLLoginFieldDelegate.h"

@implementation FLLoginFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.nextResponder becomeFirstResponder];
    return YES;
}

@end
