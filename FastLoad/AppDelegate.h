//
//  AppDelegate.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/4/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

