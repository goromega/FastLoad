//
//  FLPasswordFieldDelegate.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/5/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FLValidator;

@protocol FLFormHandler <NSObject>

@required
- (void)submitForm;

@end

@interface FLPasswordFieldDelegate : NSObject<UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet id<FLFormHandler> formHandler;
@property (nonatomic, weak) IBOutlet id<FLValidator> validator;

@end
