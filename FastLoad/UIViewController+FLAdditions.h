//
//  UIViewController+FLAdditions.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/5/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FLLoadingView.h"

@interface UIViewController (FLAdditions)

@property(nonatomic, strong)FLLoadingView *loadingView;

- (void)showLoading;
- (void)hideLoading;

@end
