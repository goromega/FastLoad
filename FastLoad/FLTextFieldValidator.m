//
//  FLTextFieldValidator.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/4/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "FLTextFieldValidator.h"

@implementation FLTextFieldValidator

- (BOOL)validate {
    self.validator.text = self.textField.text;
    return [self.validator validate];
}

@end
