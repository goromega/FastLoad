//
//  FLLoginFormValidator.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/5/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "FLLoginFormValidator.h"

@implementation FLLoginFormValidator

- (IBAction)textEditingChanged:(id)sender {
    self.downloadButton.enabled = [self validate];
}

@end
