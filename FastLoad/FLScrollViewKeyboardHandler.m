//
//  FLScroolViewKeyboardHandler.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/9/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "FLScrollViewKeyboardHandler.h"

@interface FLScrollViewKeyboardHandler()

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutletCollection(UIResponder) NSArray *responders;

@end

@implementation FLScrollViewKeyboardHandler

- (instancetype)init {
    self = [super init];
    if (self) {
        [self addObservers];
    }
    return self;
}

- (void)dealloc {
    [self removeObservers];
}

- (void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGRect keyboardEndFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [self animateWithKeyboard:notification animation:^{
        [self updateContentInsetWithBottom:keyboardEndFrame.size.height + 10];
        [self scrollFirstResponderToVisible];
    }];
}

- (void)keyboardDidHide:(NSNotification *)notification {
    [self animateWithKeyboard:notification animation:^{
        [self updateContentInsetWithBottom:0.0];
    }];
}

- (void)animateWithKeyboard:(NSNotification *)keyboardNotification animation:(void(^)())animationBlock {
    UIViewAnimationCurve animationCurve = [[[keyboardNotification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    NSTimeInterval animationDuration = [[[keyboardNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] integerValue];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    if (animationBlock) {
        animationBlock();
    }
    [UIView commitAnimations];
}

- (void)updateContentInsetWithBottom:(CGFloat)bottom {
    UIEdgeInsets contentInsets = self.scrollView.contentInset;
    contentInsets.bottom = bottom;
    self.scrollView.contentInset = contentInsets;
}

- (void)scrollFirstResponderToVisible {
    UIResponder *firstResponder = [self findFirstResponder];
    if ([firstResponder isKindOfClass:[UIView class]]) {
        UIView *activeView = (UIView *)firstResponder;
        [self.scrollView scrollRectToVisible:activeView.frame animated:NO];
    }
}

- (UIResponder *)findFirstResponder {
    for (UIResponder *responder in self.responders) {
        if ([responder isFirstResponder]) {
            return responder;
        }
    }
    return nil;
}

@end
