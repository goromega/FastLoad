//
//  FLCompositeValidator.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/4/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FLValidator.h"

@interface FLCompositeValidator : NSObject <FLValidator>

@property (nonatomic, strong) IBOutletCollection(id) NSArray *validators;

@end
