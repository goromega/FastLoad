//
//  FLLoginFormValidator.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/5/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "FLCompositeValidator.h"
#import <UIKit/UIKit.h>

@interface FLLoginFormValidator : FLCompositeValidator

@property (nonatomic, weak) IBOutlet UIButton *downloadButton;

@end
