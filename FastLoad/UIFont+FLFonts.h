//
//  UIFont+FLFonts.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/7/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (FLFonts)

#pragma mark Open Sans
+ (UIFont *) openSansWithSize:(CGFloat)size;
+ (UIFont *) openSansBoldItalicWithSize:(CGFloat)size;
+ (UIFont *) openSansItalicWithSize:(CGFloat)size;
+ (UIFont *) openSansBoldWithSize:(CGFloat)size;
+ (UIFont *) openSansLightWithSize:(CGFloat)size;
+ (UIFont *) openSansLightItalicWithSize:(CGFloat)size;
+ (UIFont *) openSansSemiboldWithSize:(CGFloat)size;
+ (UIFont *) openSansSemiboldItalicWithSize:(CGFloat)size;
+ (UIFont *) openSansExtraBoldWithSize:(CGFloat)size;

@end
