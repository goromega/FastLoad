//
//  FLTextField.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/4/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface FLTextField : UITextField

@property (nonatomic, strong) IBInspectable UIColor *lineColor;
@property (nonatomic, strong) IBInspectable UIColor *activeLineColor;
@property (nonatomic, strong) IBInspectable UIColor *placeholderColor;

@end
