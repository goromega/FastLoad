//
//  FLCompositeValidator.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/4/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "FLCompositeValidator.h"

@implementation FLCompositeValidator

- (BOOL)validate {
    BOOL isValid = YES;
    for (id<FLValidator> validator in self.validators) {
        isValid &= [validator validate];
    }
    return isValid;
}

@end
