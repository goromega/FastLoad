//
//  FLTextField.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/4/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "FLTextField.h"

static CGFloat const kFLLineWidth = 1.0;
static CGFloat const kFLTextPadding = 5.0;

@interface FLTextField ()

@property (nonatomic, weak) CALayer *lineLayer;

@end

@implementation FLTextField

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    [self configureAppearance];
    [self configureLineLayer];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self layoutLineLayer];
}

- (BOOL)becomeFirstResponder {
    BOOL result = [super becomeFirstResponder];
    [self updateLineColor];
    return result;
}

- (BOOL)resignFirstResponder {
    BOOL result = [super resignFirstResponder];
    [self updateLineColor];
    return result;
}

- (void)setLineColor:(UIColor *)lineColor {
    _lineColor = lineColor;
    [self updateLineColor];
}

- (void)setActiveLineColor:(UIColor *)activeLineColor {
    _activeLineColor = activeLineColor;
    [self updateLineColor];
    [self updateTintColor];
}

- (void)setPlaceholder:(NSString *)placeholder {
    [super setPlaceholder:placeholder];
    [self updatePlaceholder];
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor {
    _placeholderColor = placeholderColor;
    [self updatePlaceholder];
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, kFLTextPadding, 0.0);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}

- (void)configureAppearance {
    self.backgroundColor = [UIColor clearColor];
    self.borderStyle = UITextBorderStyleNone;
    [self updateTintColor];
}

- (void)configureLineLayer {
    CALayer *lineLayer = [CALayer layer];
    [self.layer addSublayer:lineLayer];
    self.lineLayer = lineLayer;
}

- (void)layoutLineLayer {
    self.lineLayer.frame = CGRectMake(0.0, self.layer.bounds.size.height - kFLLineWidth, self.layer.bounds.size.width, kFLLineWidth);
}

- (void)updateLineColor {
    self.lineLayer.backgroundColor = [([self isFirstResponder] ? self.activeLineColor : self.lineColor) CGColor];
}

- (void)updateTintColor {
    self.tintColor = self.activeLineColor;
}

- (void)updatePlaceholder {
    NSAttributedString *attributedPlaceholder = nil;
    if (self.placeholder && self.placeholderColor) {
        attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName: self.placeholderColor}];
    }
    [self setAttributedPlaceholder:attributedPlaceholder];
}

@end
