//
//  FLLoadingView.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/5/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLLoadingView : UIView

+ (FLLoadingView *)sharedView;
+ (void)showInView :(UIView *)view;
+ (void)hideView;

@end
