//
//  NSString+SHA1.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/4/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SHA1)

+(NSString *)stringToSha1:(NSString *)str;

@end
