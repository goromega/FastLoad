//
//  FLTextFieldValidator.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/4/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FLValidator.h"
#import "FLTextValidator.h"

@interface FLTextFieldValidator : NSObject <FLValidator>

@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (nonatomic, strong) IBOutlet FLTextValidator *validator;

@end
