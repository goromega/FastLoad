//
//  FLValidator.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/4/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FLValidator <NSObject>

@required
- (BOOL)validate;

@end
