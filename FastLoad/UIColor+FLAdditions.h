//
//  UIColor+FLAdditions.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/4/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (FLAdditions)

+ (UIColor *)fl_colorFromHexString:(NSString *)hexString alpha:(CGFloat)alpha;
+ (UIColor *)fl_darkGreenColor;
+ (UIColor *)fl_lightGreenColor;
+ (UIColor *)fl_backgroundColor;

@end
