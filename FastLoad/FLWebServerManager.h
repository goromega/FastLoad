//
//  FLWebServerManager.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/6/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FLWebServerManager : NSObject

+ (void)downloadImageForUsername:(NSString *)login password:(NSString *)password completion:( void (^) (id result, NSError *error))handler;

@end
