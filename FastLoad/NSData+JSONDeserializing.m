//
//  NSData+JSONDeserializing.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/6/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "NSData+JSONDeserializing.h"

@implementation NSData (JSONDeserializing)

- (id)objectFromJSONData {
    return [NSJSONSerialization JSONObjectWithData:self options:NSJSONReadingAllowFragments error:nil];
}

@end
