//
//  FLTextValidator.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/4/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FLValidator.h"

@interface FLTextValidator : NSObject <FLValidator>

@property (nonatomic, copy) NSString *text;

@end
