//
//  FLLoginFieldDelegate.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/5/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLLoginFieldDelegate : NSObject <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIResponder *nextResponder;

@end
