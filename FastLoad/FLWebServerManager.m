//
//  FLWebServerManager.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/6/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "FLWebServerManager.h"
#import "NSData+JSONDeserializing.h"

static NSString *const wsUrl = @"https://mobility.cleverlance.com/";
static NSString *const kFLDownloadMethod = @"download/bootcamp/image.php";

@implementation FLWebServerManager

+ (void)downloadImageForUsername:(NSString *)login password:(NSString *)password completion:( void (^) (id result, NSError *error))handler {
    NSDictionary *username = @{@"username" : login};
    NSMutableURLRequest *request = [self requestForPostMethod:kFLDownloadMethod withUsername:username password:password];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
                if (statusCode != 200) {
                    NSLog(@"Expected responseCode == 200; received %ld", (long)statusCode);
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        handler(nil, error);
                    });
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^(void){
                handler([data objectFromJSONData], nil);
            });
        }];
        [task resume];
    });
}

#pragma mark - Request

+ (NSMutableURLRequest *)requestForPostMethod:(NSString *)method withUsername:(NSDictionary *)username password:(NSString *) password {
    NSString* url = [NSString stringWithFormat:@"%@%@", wsUrl, method];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0f];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:password forHTTPHeaderField:@"Authorization"];
    [request setHTTPBody:[self httpBodyForParamsDictionary:username]];
    return request;
}

#pragma mark - Methods

+ (NSData *)httpBodyForParamsDictionary:(NSDictionary *)paramDictionary {
    NSMutableArray *parameterArray = [NSMutableArray array];
    [paramDictionary enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
        NSString *param = [NSString stringWithFormat:@"%@=%@", key, [self percentEscapeString:obj]];
        [parameterArray addObject:param];
    }];
    NSString *string = [parameterArray componentsJoinedByString:@"&"];
    return [string dataUsingEncoding:NSUTF8StringEncoding];
}

+ (NSString *)percentEscapeString:(NSString *)string {
//    NSString *result = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
//                                                                                 (CFStringRef)string,
//                                                                                 (CFStringRef)@" ",
//                                                                                 (CFStringRef)@":/?@!$&'()*+,;=",
//                                                                                 kCFStringEncodingUTF8));
    
    return [string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    
    
   // return [result stringByReplacingOccurrencesOfString:@" " withString:@"+"];
}

@end
