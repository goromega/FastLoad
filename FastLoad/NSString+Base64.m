//
//  NSString+Base64.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/7/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "NSString+Base64.h"

@implementation NSString (Base64)

+ (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

+ (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

@end
