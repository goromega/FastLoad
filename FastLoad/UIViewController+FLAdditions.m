//
//  UIViewController+FLAdditions.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/5/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "UIViewController+FLAdditions.h"

@implementation UIViewController (FLAdditions)

@dynamic loadingView;

- (void)showLoading {
    [FLLoadingView showInView:self.view];
}

- (void)hideLoading {
    [self.loadingView removeFromSuperview];
}

@end
