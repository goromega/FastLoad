//
//  FLLoadingView.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/5/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "FLLoadingView.h"

@implementation FLLoadingView

 UIVisualEffectView *effectView;

+ (FLLoadingView *)sharedView {
    static FLLoadingView *sharedView = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedView = [[self alloc] init];
    });
    return sharedView;
}

+ (void)showInView :(UIView *)view {
    [self sharedView];
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
    effectView.frame = [[UIScreen mainScreen] bounds];
    UIActivityIndicatorView *spiner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spiner.center = effectView.center;
    [spiner startAnimating];
    [effectView addSubview:spiner];
    [view addSubview:effectView];
}

+ (void)hideView {
    [effectView removeFromSuperview];
}

@end
