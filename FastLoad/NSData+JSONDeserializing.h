//
//  NSData+JSONDeserializing.h
//  FastLoad
//
//  Created by Gor Hakobyan on 9/6/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (JSONDeserializing)

- (id)objectFromJSONData;

@end
