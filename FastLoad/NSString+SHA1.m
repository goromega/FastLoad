//
//  NSString+SHA1.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/4/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "NSString+SHA1.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (SHA1)

+(NSString *)stringToSha1:(NSString *)string{
    if(string) {
        const char *cStr = [string UTF8String];
        unsigned char result[CC_SHA512_DIGEST_LENGTH];
        CC_SHA1(cStr, (int)strlen(cStr), result);
        NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH];
        for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
            [output appendFormat:@"%02x", result[i]];
        return output;
    }
    return nil;
}

@end
