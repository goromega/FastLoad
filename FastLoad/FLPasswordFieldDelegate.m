//
//  FLPasswordFieldDelegate.m
//  FastLoad
//
//  Created by Gor Hakobyan on 9/5/16.
//  Copyright © 2016 Gor Hakobyan. All rights reserved.
//

#import "FLPasswordFieldDelegate.h"
#import "FLValidator.h"

@implementation FLPasswordFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self performSelector:@selector(performAction) withObject:nil afterDelay:0.0];
    return YES;
}

- (void)performAction {
    if ([self.validator validate]) {
        [self.formHandler submitForm];
    }
}

@end
